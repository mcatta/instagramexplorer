# README #

Created with Android Studio 2.3

buildToolsVersion 25.0.2

gradle 2.3

### Library used: ###

* Android Support Library
* RecyclerView v7
* CardView v7
* Realm: to save media on a nosql db to cache data
* Square OkHttp for http request
* Square Picasso to load picture and cache them
* ButterKnife view binding
* Google Gson to parse json data
* RXJava 2