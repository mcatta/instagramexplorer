package eu.marcocattaneo.instagramexplorer.interactor;


import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public abstract class UseCase<T, Params> {

    private final CompositeDisposable disposables;

    private final Scheduler subcribeScheduler;

    private final Scheduler observerScheduler;

    public UseCase(Scheduler subcribeScheduler, Scheduler observerScheduler) {
        this.disposables = new CompositeDisposable();
        this.subcribeScheduler = subcribeScheduler;
        this.observerScheduler = observerScheduler;
    }

    public abstract Observable<T> buildUseCaseObservable(Params params);

    /**
     * Execute the observable
     * @param observer
     * @param params
     * @return
     */
    public Observable<T> execute(DisposableObserver<T> observer, Params params) {
        final Observable<T> observable = this.buildUseCaseObservable(params)
                .subscribeOn(subcribeScheduler)
                .observeOn(observerScheduler);

        // Subscribe
        addDisposable(observable.subscribeWith(observer));

        return observable;
    }

    /**
     * Dispose disposables
     */
    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    /**
     * Add disposable
     * @param disposable
     */
    protected void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

}
