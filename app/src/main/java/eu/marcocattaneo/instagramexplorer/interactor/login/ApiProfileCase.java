package eu.marcocattaneo.instagramexplorer.interactor.login;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.instagramsession.InstagramSession;
import eu.marcocattaneo.instagramexplorer.data.user.User;
import eu.marcocattaneo.instagramexplorer.interactor.UseCase;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramSessionRepositoryImpl;
import eu.marcocattaneo.instagramexplorer.repository.login.LoginRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramLoginDatasource;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ApiProfileCase extends UseCase<User, Void> {

    private LoginRepository loginRepository;

    @Inject
    public ApiProfileCase(InstagramLoginDatasource instagramLoginDatasource) {
        super(Schedulers.io(), AndroidSchedulers.mainThread());
        this.loginRepository = instagramLoginDatasource;
    }

    @Override
    public Observable<User> buildUseCaseObservable(Void aVoid) {
        return loginRepository.getUser().map(new Function<User, User>() {
            @Override
            public User apply(@NonNull User user) throws Exception {

                // Save profile info
                InstagramSessionRepository instagramSession = new InstagramSessionRepositoryImpl();
                InstagramSession session = instagramSession.getSetting();

                session.setUserName(user.getFull_name());
                session.setProfilePicture(user.getProfile_picture());

                instagramSession.saveSettings(session);
                return user;
            }
        });
    }

    @Override
    public void dispose() {
        super.dispose();
    }

}
