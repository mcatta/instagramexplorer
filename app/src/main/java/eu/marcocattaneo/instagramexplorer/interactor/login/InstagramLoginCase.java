package eu.marcocattaneo.instagramexplorer.interactor.login;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.interactor.UseCase;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramLoginDatasource;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class InstagramLoginCase extends UseCase<String, InstagramLoginCase.Params> {

    private InstagramLoginDatasource loginDatasource;

    @Inject
    public InstagramLoginCase(InstagramLoginDatasource loginDatasource) {
        super(Schedulers.io(), AndroidSchedulers.mainThread());

        this.loginDatasource = loginDatasource;
    }

    /**
     * Require Instagram API Session token
     * @param params
     * @return
     */
    @Override
    public Observable<String> buildUseCaseObservable(Params params) {
        return loginDatasource.getInstagramToken(params.code, params.clientid, params.secret, params.callback).map(new Function<String, String>() {

            @Override
            public String apply(@NonNull String token) throws Exception {
                return token;
            }

        });
    }

    public static class Params {

        String code;

        String secret;

        String clientid;

        String callback;

        public Params(String code, String clientid, String secret, String callback) {
            this.code = code;
            this.secret = secret;
            this.clientid = clientid;
            this.callback = callback;
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
