package eu.marcocattaneo.instagramexplorer.interactor.media;

import java.util.List;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.interactor.UseCase;
import eu.marcocattaneo.instagramexplorer.repository.media.MediaRepository;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.LocalStorageDatasource;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.RemoteDatasource;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ApiMediaListCase extends UseCase<List<Media>, Void> {

    private MediaRepository instagramRepository;
    private MediaRepository localstorageRepository;

    @Inject
    public ApiMediaListCase(RemoteDatasource remoteDatasource, LocalStorageDatasource localStorageDatasource) {
        super(Schedulers.io(), AndroidSchedulers.mainThread());
        this.instagramRepository = remoteDatasource;
        this.localstorageRepository = localStorageDatasource;
    }

    @Override
    public Observable<List<Media>> buildUseCaseObservable(Void aVoid) {
        return instagramRepository.getAll()
                /*
                 * Save api array on db
                 */
                .map(new Function<List<Media>, List<Media>>() {
            @Override
            public List<Media> apply(@NonNull List<Media> mediaList) throws Exception {

                if (mediaList != null && mediaList.size() > 0) {
                    // Write for local
                    localstorageRepository.eraseAll();
                    localstorageRepository.writeAll(mediaList);
                }

                return mediaList;
            }
        });
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
