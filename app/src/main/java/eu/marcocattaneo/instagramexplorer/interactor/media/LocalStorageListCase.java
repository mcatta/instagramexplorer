package eu.marcocattaneo.instagramexplorer.interactor.media;

import java.util.List;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.interactor.UseCase;
import eu.marcocattaneo.instagramexplorer.repository.media.MediaRepository;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.LocalStorageDatasource;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class LocalStorageListCase extends UseCase<List<Media>, Void> {

    private MediaRepository mediaRepository;

    @Inject
    public LocalStorageListCase(LocalStorageDatasource localStorageDatasource) {
        // Do login in doBackground, response on UI thread
        super(AndroidSchedulers.mainThread(), AndroidSchedulers.mainThread());

        this.mediaRepository = localStorageDatasource;
    }

    @Override
    public Observable<List<Media>> buildUseCaseObservable(Void aVoid) {
        return mediaRepository.getAll();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
