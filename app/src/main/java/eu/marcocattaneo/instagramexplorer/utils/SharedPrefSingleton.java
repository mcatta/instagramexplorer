package eu.marcocattaneo.instagramexplorer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPrefSingleton {

    private static SharedPreferences mSharedPreferences;

    /**
     * Initialize SharedPreferences singleton
     * @param context
     */
    public static void init(Context context) {
        if (mSharedPreferences == null)
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences getInstance() {
        return mSharedPreferences;
    }

}
