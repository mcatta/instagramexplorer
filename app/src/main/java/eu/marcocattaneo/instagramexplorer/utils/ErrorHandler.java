package eu.marcocattaneo.instagramexplorer.utils;

import android.content.Context;

import java.net.UnknownHostException;

import eu.marcocattaneo.instagramexplorer.R;

/**
 * Created by marcocat on 14/04/17.
 */

public class ErrorHandler {

    public static String getErrorMessage(Context context, Throwable e) {

        if (e instanceof UnknownHostException) {
            return context.getResources().getString(R.string.err_no_network);
        } else {
            return null;
        }

    }

}
