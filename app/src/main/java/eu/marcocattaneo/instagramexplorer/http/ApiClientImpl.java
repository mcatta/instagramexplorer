package eu.marcocattaneo.instagramexplorer.http;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiClientImpl implements ApiClient {

    private OkHttpClient client;

    @Inject
    public ApiClientImpl(OkHttpClient okHttpClient) {
        client = okHttpClient;
    }

    /**
     * Make get request
     * @param url
     * @param headers
     * @return
     * @throws IOException
     */
    public String get(@NonNull String url, @Nullable Map<String, String> headers) throws IOException {
        Request.Builder requestBuilder = new Request.Builder()
                .url(url);

        // Add header
        if (headers != null) {
            requestBuilder = addHeaderValues(headers, requestBuilder);
        }

        Response response = client.newCall(requestBuilder.build()).execute();
        return response.body().string();
    }

    /**
     * Add headers key to Request.Build
     * @param header
     * @param requestBuilder
     * @return
     */
    private Request.Builder addHeaderValues(Map<String, String> header, Request.Builder requestBuilder) {
        for(String key : header.keySet()) {
            requestBuilder.addHeader(key, header.get(key));
        }

        return requestBuilder;
    }

    /**
     * Make post request
     * @param url
     * @param body
     * @param headers
     * @return
     */
    @Override
    public String post(@NonNull String url, @Nullable RequestBody body, @Nullable Map<String, String> headers) throws IOException {

        Request.Builder requestBuilder = new Request.Builder()
                .post(body)
                .url(url);

        // Add header
        if (headers != null) {
            requestBuilder = addHeaderValues(headers, requestBuilder);
        }

        Response response = client.newCall(requestBuilder.build()).execute();
        return response.body().string();
    }

}
