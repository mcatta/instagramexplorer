package eu.marcocattaneo.instagramexplorer.http;

import org.json.JSONObject;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.data.user.User;

public interface InstagramAPI {

    String ENDPOINT = "https://api.instagram.com/v1";

    String AUTH_TOKEN = "https://api.instagram.com/oauth/access_token";

    String AUTH_LOGIN = "https://api.instagram.com/oauth/authorize/";

    /**
     * Return user profile
     * @return
     */
    User getUserSelf() throws Exception;

    /**
     * Return user recent media
     * @return
     */
    List<Media> getUserMedia() throws Exception;

    /**
     * Return authentication token to use API
     * @return
     * @throws Exception
     */
    JSONObject getAuthToken(String clientId, String clientSecret, String clientCallback, String code) throws Exception;

}
