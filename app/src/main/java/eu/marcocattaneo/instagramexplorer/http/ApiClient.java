package eu.marcocattaneo.instagramexplorer.http;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.Map;

import okhttp3.RequestBody;

public interface ApiClient {

    /**
     * Http get request
     * @param url
     * @param headers
     * @return
     * @throws IOException
     */
    String get(@NonNull String url, @Nullable Map<String, String> headers) throws IOException;

    /**
     * Http post request
     * @param url
     * @param body
     * @param headers
     * @return
     * @throws IOException
     */
    String post(@NonNull String url, @Nullable RequestBody body, @Nullable Map<String, String> headers) throws IOException;

}
