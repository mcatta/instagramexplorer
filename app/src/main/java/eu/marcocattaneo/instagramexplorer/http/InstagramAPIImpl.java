package eu.marcocattaneo.instagramexplorer.http;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.instagramsession.InstagramSession;
import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.data.response.MediaResponse;
import eu.marcocattaneo.instagramexplorer.data.response.UserResponse;
import eu.marcocattaneo.instagramexplorer.data.user.User;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramSessionRepositoryImpl;
import eu.marcocattaneo.instagramexplorer.utils.JSONMapper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class InstagramAPIImpl implements InstagramAPI {

    private ApiClient apiClient;
    private InstagramSessionRepository instagramSessionRepository;

    @Inject
    public InstagramAPIImpl(ApiClientImpl apiClient, InstagramSessionRepository instagramSessionRepository) {
        this.apiClient = apiClient;
        this.instagramSessionRepository = instagramSessionRepository;
    }

    @Override
    public User getUserSelf() throws Exception {
        String json = apiClient.get(InstagramAPI.ENDPOINT + "/users/self" + getToken(), null);

        UserResponse mediaResponse = JSONMapper.fromJson(json, UserResponse.class);

        return mediaResponse != null ? mediaResponse.getData() : null;
    }

    @Override
    public List<Media> getUserMedia() throws Exception {

        String json = apiClient.get(InstagramAPI.ENDPOINT + "/users/self/media/recent" + getToken(), null);

        MediaResponse mediaResponse = JSONMapper.fromJson(json, MediaResponse.class);

        return mediaResponse != null ? mediaResponse.getData() : null;
    }

    @Override
    public JSONObject getAuthToken(String clientId, String clientSecret, String clientCallback, String code) throws Exception {

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("client_id", clientId)
                .addFormDataPart("client_secret", clientSecret)
                .addFormDataPart("grant_type", "authorization_code")
                .addFormDataPart("redirect_uri", clientCallback)
                .addFormDataPart("code", code).build();

        String json = apiClient.post("https://api.instagram.com/oauth/access_token", requestBody, null);

        return new JSONObject(json);
    }

    /**
     * Return instagram session token
     * @return
     */
    private String getToken() {
        InstagramSession instagramSession = instagramSessionRepository.getSetting();
        return instagramSession != null ? "/?access_token=" + instagramSession.getToken() : "";
    }
}
