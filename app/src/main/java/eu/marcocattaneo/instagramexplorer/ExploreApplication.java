package eu.marcocattaneo.instagramexplorer;

import android.app.Application;

import eu.marcocattaneo.instagramexplorer.data.di.components.AppComponent;
import eu.marcocattaneo.instagramexplorer.data.di.components.DaggerAppComponent;
import eu.marcocattaneo.instagramexplorer.data.di.modules.AppModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.CaseModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.NetModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.RepositoryModule;
import eu.marcocattaneo.instagramexplorer.utils.SharedPrefSingleton;
import io.realm.Realm;
import io.realm.RealmConfiguration;


public class ExploreApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initRealm();
        SharedPrefSingleton.init(this);

        initDagger();
    }

    private void initDagger() {

        appComponent = DaggerAppComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .repositoryModule(new RepositoryModule())
                .caseModule(new CaseModule())
                .netModule(new NetModule())
                .build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    /**
     * Init realm DB
     */
    private void initRealm() {
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .name(getString(R.string.realm_name))
                .build();

        Realm.setDefaultConfiguration(config);
    }
}
