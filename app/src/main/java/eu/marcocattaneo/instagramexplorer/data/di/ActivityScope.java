package eu.marcocattaneo.instagramexplorer.data.di;

import javax.inject.Scope;

/**
 * Created by marcocat on 11/05/17.
 */

@Scope
public @interface ActivityScope {
}
