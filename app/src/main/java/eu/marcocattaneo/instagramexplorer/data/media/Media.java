package eu.marcocattaneo.instagramexplorer.data.media;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Media extends RealmObject {

    @PrimaryKey
    private String id;

    private Counter comments;

    private Counter likes;

    private String link;

    private MediaMap images;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Counter getComments() {
        return comments;
    }

    public void setComments(Counter comments) {
        this.comments = comments;
    }

    public Counter getLikes() {
        return likes;
    }

    public void setLikes(Counter likes) {
        this.likes = likes;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public MediaMap getImages() {
        return images;
    }

    public void setImages(MediaMap images) {
        this.images = images;
    }
}
