package eu.marcocattaneo.instagramexplorer.data.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.marcocattaneo.instagramexplorer.interactor.login.ApiProfileCase;
import eu.marcocattaneo.instagramexplorer.interactor.login.InstagramLoginCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.ApiMediaListCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.LocalStorageListCase;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramLoginDatasource;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.LocalStorageDatasource;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.RemoteDatasource;

/**
 * Created by marcocat on 11/05/17.
 */

@Module
public class CaseModule {

    public CaseModule() {
    }

    @Provides
    @Singleton
    InstagramLoginCase provideInstagramLoginCase(InstagramLoginDatasource loginDatasource) {
        return new InstagramLoginCase(loginDatasource);
    }

    @Provides
    @Singleton
    ApiProfileCase provideApiProfileCase(InstagramLoginDatasource instagramLoginCase) {
        return new ApiProfileCase(instagramLoginCase);
    }

    @Provides
    @Singleton
    ApiMediaListCase provideApiMediaListCase(RemoteDatasource remoteDatasource, LocalStorageDatasource localStorageDatasource) {
        return new ApiMediaListCase(remoteDatasource, localStorageDatasource);
    }

    @Provides
    @Singleton
    LocalStorageListCase provideLocalStorageListCase(LocalStorageDatasource localStorageDatasource) {
        return new LocalStorageListCase(localStorageDatasource);
    }

}
