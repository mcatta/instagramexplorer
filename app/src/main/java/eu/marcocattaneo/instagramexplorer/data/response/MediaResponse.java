package eu.marcocattaneo.instagramexplorer.data.response;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;

public class MediaResponse {

    private List<Media> data;

    public List<Media> getData() {
        return data;
    }
}
