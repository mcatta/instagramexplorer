package eu.marcocattaneo.instagramexplorer.data.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.marcocattaneo.instagramexplorer.http.ApiClientImpl;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPI;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPIImpl;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramLoginDatasource;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramSessionRepositoryImpl;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.LocalStorageDatasource;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.RemoteDatasource;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainContract;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainPresenter;

@Module
public class RepositoryModule {

    public RepositoryModule() {}

    @Provides
    @Singleton
    InstagramSessionRepository providesInstagramSessionRepository() {
        return new InstagramSessionRepositoryImpl();
    }

    @Provides
    @Singleton
    InstagramAPI provideInstagramAPI(ApiClientImpl apiClient, InstagramSessionRepository instagramSessionRepository) {
        return new InstagramAPIImpl(apiClient, instagramSessionRepository);
    }

    @Provides
    @Singleton
    InstagramLoginDatasource providesInstagramLoginDatasource(InstagramAPI instagramAPI) {
        return new InstagramLoginDatasource(instagramAPI);
    }

    @Provides
    @Singleton
    RemoteDatasource provideRemoteDatasource(InstagramAPI instagramAPI) {
        return new RemoteDatasource(instagramAPI);
    }

    @Provides
    @Singleton
    LocalStorageDatasource provideLocalStorageDatasource() {
        return new LocalStorageDatasource();
    }

}
