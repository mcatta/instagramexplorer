package eu.marcocattaneo.instagramexplorer.data.di.components;

import dagger.Component;
import eu.marcocattaneo.instagramexplorer.data.di.PerActivity;
import eu.marcocattaneo.instagramexplorer.data.di.modules.MainModule;
import eu.marcocattaneo.instagramexplorer.ui.presenter.gallery.GalleryActivity;
import eu.marcocattaneo.instagramexplorer.ui.presenter.list.ListFragment;
import eu.marcocattaneo.instagramexplorer.ui.presenter.login.LoginFragment;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainActivity;

/**
 * Created by marcocat on 11/05/17.
 */

@PerActivity
@Component(dependencies = {AppComponent.class}, modules = {MainModule.class})
public interface ActivityComponent {

    void inject(LoginFragment loginFragment);

    void inject(MainActivity activity);

    void inject(GalleryActivity galleryActivity);

    void inject(ListFragment listFragment);
}
