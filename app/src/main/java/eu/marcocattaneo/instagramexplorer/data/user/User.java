package eu.marcocattaneo.instagramexplorer.data.user;

public class User {

    private String id;

    private String full_name;

    private String profile_picture;

    public String getId() {
        return id;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }
}
