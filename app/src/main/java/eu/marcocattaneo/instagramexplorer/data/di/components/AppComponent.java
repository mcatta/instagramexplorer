package eu.marcocattaneo.instagramexplorer.data.di.components;

import javax.inject.Singleton;

import dagger.Component;
import eu.marcocattaneo.instagramexplorer.data.di.modules.AppModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.CaseModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.MainModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.NetModule;
import eu.marcocattaneo.instagramexplorer.data.di.modules.RepositoryModule;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPI;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPIImpl;
import eu.marcocattaneo.instagramexplorer.interactor.login.ApiProfileCase;
import eu.marcocattaneo.instagramexplorer.interactor.login.InstagramLoginCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.ApiMediaListCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.LocalStorageListCase;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramLoginDatasource;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramSessionRepositoryImpl;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.LocalStorageDatasource;
import eu.marcocattaneo.instagramexplorer.repository.media.datasource.RemoteDatasource;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainActivity;
import okhttp3.OkHttpClient;

@Singleton
@Component(modules = {NetModule.class, AppModule.class, CaseModule.class, RepositoryModule.class})
public interface AppComponent {

    InstagramLoginCase instagramLoginCase();

    ApiProfileCase apiProfileCase();

    InstagramSessionRepository instagramSessionRepository();

    InstagramLoginDatasource instagramLoginDatasource();

    ApiMediaListCase apiMediaListCase();

    LocalStorageListCase localStorageListCase();

    RemoteDatasource remoteDatasource();

    LocalStorageDatasource localStorageDatasource();

    OkHttpClient okHttpClient();

    InstagramAPI instagramAPIImpl();

}
