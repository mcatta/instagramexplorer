package eu.marcocattaneo.instagramexplorer.data.media;

import io.realm.RealmObject;

public class Counter extends RealmObject {

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
