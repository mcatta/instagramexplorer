package eu.marcocattaneo.instagramexplorer.data.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.marcocattaneo.instagramexplorer.data.di.PerActivity;
import eu.marcocattaneo.instagramexplorer.interactor.login.ApiProfileCase;
import eu.marcocattaneo.instagramexplorer.interactor.login.InstagramLoginCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.ApiMediaListCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.LocalStorageListCase;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.ui.presenter.gallery.GalleryContract;
import eu.marcocattaneo.instagramexplorer.ui.presenter.gallery.GalleryPresenter;
import eu.marcocattaneo.instagramexplorer.ui.presenter.list.ListContract;
import eu.marcocattaneo.instagramexplorer.ui.presenter.list.ListPresenter;
import eu.marcocattaneo.instagramexplorer.ui.presenter.login.LoginContract;
import eu.marcocattaneo.instagramexplorer.ui.presenter.login.LoginPresenter;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainContract;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainPresenter;

/**
 * Created by marcocat on 11/05/17.
 */

@Module
public class MainModule {

    public MainModule() {
    }

    @PerActivity
    @Provides
    MainContract.MainPresenter provideMainPresenter(InstagramSessionRepository instagramSessionRepository) {
        return new MainPresenter(instagramSessionRepository);
    }

    @PerActivity
    @Provides
    LoginContract.LoginPresenter provideLoginPresenter(InstagramLoginCase instagramLoginCase, ApiProfileCase apiProfileCase) {
        return new LoginPresenter(instagramLoginCase, apiProfileCase);
    }

    @PerActivity
    @Provides
    ListContract.ListPresenter provideListPresenter(ApiMediaListCase apiListCase, LocalStorageListCase localStorageListCase) {
        return new ListPresenter(apiListCase, localStorageListCase);
    }

    @PerActivity
    @Provides
    GalleryContract.GalleryPresenter provideGalleryPresenter(LocalStorageListCase localStorageListCase) {
        return new GalleryPresenter(localStorageListCase);
    }

}
