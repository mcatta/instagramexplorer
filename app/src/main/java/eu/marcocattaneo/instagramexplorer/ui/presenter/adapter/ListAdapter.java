package eu.marcocattaneo.instagramexplorer.ui.presenter.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.marcocattaneo.instagramexplorer.R;
import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.widget.InstagramImageView;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.PictureViewHolders> {

    private List<Media> mediaList;

    private OnItemClickListener onItemClickListener;

    public ListAdapter(List<Media> mediaList) {
        this.mediaList = mediaList;
    }

    /**
     * Return specific media
     * @param position
     * @return
     */
    public Media getItem(int position) {
        return this.mediaList != null ? this.mediaList.get(position) : null;
    }

    @Override
    public PictureViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list, null);
        return new PictureViewHolders(view);
    }

    @Override
    public void onBindViewHolder(PictureViewHolders holder, int position) {
        Media media = getItem(position);

        holder.thumb.setErrorResouceImage(R.drawable.ic_network_black_48dp);
        holder.thumb.setPlaceholderResouceImage(R.drawable.ic_hourglass_full_black_48dp);

        holder.thumb.setSrc(
                media.getImages().getLow_resolution().getUrl(),
                media.getImages().getLow_resolution().getWidth(),
                media.getImages().getLow_resolution().getHeight());
        holder.bindClickListener(onItemClickListener, position);

        holder.likeCount.setText(String.valueOf(media.getLikes().getCount()));
        holder.messageCount.setText(String.valueOf(media.getComments().getCount()));
    }

    @Override
    public int getItemCount() {
        return mediaList != null ? mediaList.size() : 0;
    }

    /**
     * Change data
     * @param mediaList
     */
    public void swapData(@Nullable List<Media> mediaList) {
        if (mediaList == null)
            return;
        int currentSize = this.mediaList.size();
        this.mediaList.clear();
        this.mediaList.addAll(mediaList);

        // Notify removed items
        notifyItemRangeRemoved(0, currentSize);

        // Notify added items
        notifyItemRangeInserted(0, mediaList.size());
    }

    /**
     * Set on item click listener
     * @param onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class PictureViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnItemClickListener onItemClickListener;

        private int position;

        @BindView(R.id.picture_iv)
        InstagramImageView thumb;

        @BindView(R.id.like_count)
        TextView likeCount;

        @BindView(R.id.msg_count)
        TextView messageCount;

        public PictureViewHolders(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        public void bindClickListener(OnItemClickListener onItemClickListener, int position) {
            this.onItemClickListener = onItemClickListener;
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            // Contact interface on click
            if (onItemClickListener != null)
                onItemClickListener.onClick(view, mediaList.get(position));
        }
    }

    public interface OnItemClickListener {

        void onClick(View view, Media media);

    }

}
