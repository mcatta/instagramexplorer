package eu.marcocattaneo.instagramexplorer.ui.presenter.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.marcocattaneo.instagramexplorer.R;

public abstract class BaseFragment extends Fragment {

    private Unbinder mUnbinder;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initToolbar((Toolbar) view.findViewById(R.id.toolbar));
        mUnbinder = ButterKnife.bind(this, view);
    }

    /**
     * Initialize Toolbar if exists
     * @param toolbar
     */
    public void initToolbar(@Nullable Toolbar toolbar) {

        if (toolbar == null)
            return;

        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}
