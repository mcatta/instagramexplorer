package eu.marcocattaneo.instagramexplorer.ui.presenter.gallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import butterknife.BindView;
import eu.marcocattaneo.instagramexplorer.R;
import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BaseFragment;
import eu.marcocattaneo.instagramexplorer.ui.widget.InstagramImageView;

public class GalleryFragment extends BaseFragment {

    private static String EXTRA_MEDIA = "media.gallery.item";

    public static GalleryFragment newInstance(Media media) {

        SerializableMedia serializableMedia = new SerializableMedia();
        serializableMedia.srcImage = media.getImages().getStandard_resolution().getUrl();
        serializableMedia.srcWidth = media.getImages().getStandard_resolution().getWidth();
        serializableMedia.srcHeight = media.getImages().getStandard_resolution().getHeight();

        serializableMedia.likesCount = media.getLikes().getCount();
        serializableMedia.commentsCount = media.getComments().getCount();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_MEDIA, serializableMedia);

        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.fullscreen_imageview)
    public InstagramImageView fullScreenImageView;

    private SerializableMedia serializableMedia;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        serializableMedia = (SerializableMedia) getArguments().getSerializable(EXTRA_MEDIA);

        fullScreenImageView.setErrorResouceImage(R.drawable.ic_network_white_48dp);
        fullScreenImageView.setPlaceholderResouceImage(R.drawable.ic_hourglass_full_white_48dp);

        fullScreenImageView.setSrc(serializableMedia.srcImage, serializableMedia.srcWidth, serializableMedia.srcHeight);

    }

    private static class SerializableMedia implements Serializable {

        private String srcImage;

        private int srcWidth, srcHeight, commentsCount, likesCount;

    }

}
