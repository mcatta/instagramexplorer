package eu.marcocattaneo.instagramexplorer.ui.presenter.gallery;

import java.util.List;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.interactor.media.LocalStorageListCase;
import io.reactivex.observers.DisposableObserver;

public class GalleryPresenter implements GalleryContract.GalleryPresenter {

    private GalleryContract.View mView;

    private String startMediaId;

    private List<Media> currentMediaList;

    private LocalStorageListCase localListCase;

    @Inject
    public GalleryPresenter(LocalStorageListCase localListCase) {
        this.localListCase = localListCase;
    }

    @Override
    public void onAttach(GalleryContract.View view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    @Override
    public void loadMedias() {
        this.localListCase.execute(new MediaListObserver(), null);
    }

    @Override
    public void setStartId(String id) {
        this.startMediaId = id;
    }

    @Override
    public void onViewpagerChange(int position) {
        Media media = currentMediaList.get(position);

        mView.setMediaDetail(media.getLikes().getCount(), media.getComments().getCount());
    }

    /**
     * Observer for Media List
     */
    private final class MediaListObserver extends DisposableObserver<List<Media>> {

        @Override
        public void onNext(List<Media> medias) {
            currentMediaList = medias;
            int startAt = 0;
            for (int i = 0; i < medias.size(); i++) {
                if (medias.get(i).getId().equals(startMediaId)) {
                    startAt = i;
                    break;
                }
            }
            mView.initAdapter(medias);
            mView.moveToPosition(startAt);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }

}
