package eu.marcocattaneo.instagramexplorer.ui.presenter.main;

import android.os.Bundle;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.ExploreApplication;
import eu.marcocattaneo.instagramexplorer.R;
import eu.marcocattaneo.instagramexplorer.data.di.components.ActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.components.AppComponent;
import eu.marcocattaneo.instagramexplorer.data.di.components.DaggerActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.modules.MainModule;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BaseActivity;
import eu.marcocattaneo.instagramexplorer.ui.presenter.list.ListFragment;
import eu.marcocattaneo.instagramexplorer.ui.presenter.login.LoginFragment;

public class MainActivity extends BaseActivity implements MainContract.View {

    @Inject
    public MainContract.MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appComponent( ((ExploreApplication) getApplication()).getAppComponent())
                .mainModule(new MainModule())
                .build();

        activityComponent.inject(this);

        mainPresenter.onAttach(this);

        // Verify Instagram token
        mainPresenter.checkSession();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.onDetach();
    }

    @Override
    public void openLoginFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance()).commit();
    }

    @Override
    public void openGalleryFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, ListFragment.newInstance()).commit();
    }
}
