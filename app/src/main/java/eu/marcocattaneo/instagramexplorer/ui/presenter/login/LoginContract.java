package eu.marcocattaneo.instagramexplorer.ui.presenter.login;

import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BasePresenter;

public interface LoginContract {

    interface View {

        /**
         * Show snackbar
         * @param message
         */
        void showSnackbar(String message);

        /**
         * Show loading dialog for user and pass
         */
        void showLoginDialog();

        /**
         * Login is ok
         * @param  avatarUrl url of instagarm profile
         */
        void loginOK(String loginName, String avatarUrl);

        /**
         * Contacts this method when login has error
         */
        void loginKO();

    }

    interface LoginPresenter extends BasePresenter<View> {

        /**
         * Start login
         */
        void login();

        /**
         * Pass login paramters for token
         * @param code
         * @param clientid
         * @param secret
         * @param callback
         */
        void passInstagramCode(String code, String clientid, String secret, String callback);

    }

}
