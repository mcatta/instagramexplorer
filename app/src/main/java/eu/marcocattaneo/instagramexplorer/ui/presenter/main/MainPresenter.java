package eu.marcocattaneo.instagramexplorer.ui.presenter.main;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.di.PerActivity;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramSessionRepositoryImpl;

@PerActivity
public class MainPresenter implements MainContract.MainPresenter {

    private MainContract.View view;

    private InstagramSessionRepository instagramSessionRepository;

    @Inject
    public MainPresenter(InstagramSessionRepository instagramSessionRepository) {
        this.instagramSessionRepository = instagramSessionRepository;
    }

    @Override
    public void onAttach(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void onDetach() {
        view = null;
    }

    @Override
    public void checkSession() {

        if (instagramSessionRepository.getSetting() == null)
            view.openLoginFragment();
        else
            view.openGalleryFragment();

    }
}
