package eu.marcocattaneo.instagramexplorer.ui.presenter.list;

import java.util.List;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.di.PerActivity;
import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.interactor.media.ApiMediaListCase;
import eu.marcocattaneo.instagramexplorer.interactor.media.LocalStorageListCase;
import eu.marcocattaneo.instagramexplorer.utils.ErrorHandler;
import io.reactivex.observers.DisposableObserver;

@PerActivity
public class ListPresenter implements ListContract.ListPresenter {

    private ListContract.View mView;

    private LocalStorageListCase localListCase;

    private ApiMediaListCase apiListCase;

    private boolean mfirstFetchDone = false;

    private boolean mLoadFirstLocal = false;

    @Inject
    public ListPresenter(ApiMediaListCase apiListCase, LocalStorageListCase localStorageListCase) {
        this.localListCase = localStorageListCase;
        this.apiListCase = apiListCase;
    }

    @Override
    public void onAttach(ListContract.View view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        localListCase.dispose();
        apiListCase.dispose();
        mView = null;
    }

    @Override
    public void loadLocalMedia() {
        localListCase.execute(new MediaListObserver(), null);
    }

    @Override
    public void fetchRemoteMedia(boolean force) {

        // To prevent remote fetch every time reume fragment
        if (force || !mfirstFetchDone) {

            // Load local first
            mView.startRefresh();
            apiListCase.execute(new MediaListObserver(), null);
            mfirstFetchDone = true;
        }
    }

    /**
     * Observer for Media List
     */
    private final class MediaListObserver extends DisposableObserver<List<Media>> {

        @Override
        public void onNext(List<Media> medias) {
            mView.setData(medias);
        }

        @Override
        public void onError(Throwable e) {

            String msg = ErrorHandler.getErrorMessage(mView.context(), e);
            if (msg != null)
                mView.showSnackbar(msg);

            mView.stopRefresh();
        }

        @Override
        public void onComplete() {
            mView.stopRefresh();
        }
    }

}
