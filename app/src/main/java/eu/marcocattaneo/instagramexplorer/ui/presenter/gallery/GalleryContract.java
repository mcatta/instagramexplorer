package eu.marcocattaneo.instagramexplorer.ui.presenter.gallery;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BasePresenter;

public interface GalleryContract {

    interface View {

        /**
         * Move to specific position on viewpager
         * @param position
         */
        void moveToPosition(int position);

        /**
         * Fill ViewPager adapter
         * @param mediaList
         */
        void initAdapter(List<Media> mediaList);

        /**
         * Show media information
         * @param likesCount
         * @param commentCount
         */
        void setMediaDetail(int likesCount, int commentCount);

    }

    interface GalleryPresenter extends BasePresenter<View> {

        /**
         * Fetch media from local
         */
        void loadMedias();

        /**
         * Set media to show
         * @param id
         */
        void setStartId(String id);

        /**
         *
         * @param position
         */
        void onViewpagerChange(int position);

    }

}
