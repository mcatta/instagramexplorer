package eu.marcocattaneo.instagramexplorer.ui.presenter.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.presenter.gallery.GalleryFragment;

public class ViewPagerGalleryAdapter extends FragmentPagerAdapter {

    private List<Media> mediaList;

    public ViewPagerGalleryAdapter(FragmentManager fm, List<Media> mediaList) {
        super(fm);
        this.mediaList = mediaList;
    }

    @Override
    public int getCount() {
        return mediaList != null ? mediaList.size() : 0;
    }

    @Override
    public Fragment getItem(int position) {
        return GalleryFragment.newInstance(mediaList.get(position));
    }

}
