package eu.marcocattaneo.instagramexplorer.ui.widget;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class InstagramImageView extends AppCompatImageView implements ImageViewInterface {

    private @DrawableRes int placeHolder = 0;

    private @DrawableRes int errorImage = 0;

    public InstagramImageView(Context context) {
        super(context);
    }

    public InstagramImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InstagramImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSrc(final String srcMedia, final int srcWidth, final int srcHeight) {

        // Resize
        ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            public boolean onPreDraw() {
                getViewTreeObserver().removeOnPreDrawListener(this);


                int newWidth = getMeasuredWidth();
                int newHeight =  (newWidth * srcHeight) / srcWidth;

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
                layoutParams.width = newWidth;
                layoutParams.height = newHeight;
                setLayoutParams(layoutParams);
                RequestCreator requestCreator = Picasso.with(getContext()).load(srcMedia)
                        .resize(newWidth, newHeight);

                // Set placeholder
                if (placeHolder > 0)
                    requestCreator.placeholder(placeHolder);
                if (errorImage > 0)
                    requestCreator.error(errorImage);
                requestCreator.into(InstagramImageView.this);

                return true;
            }

        });

    }

    @Override
    public void setErrorResouceImage(@DrawableRes int resouceImage) {
        this.errorImage = resouceImage;
    }

    @Override
    public void setPlaceholderResouceImage(@DrawableRes int resouceImage) {
        this.placeHolder = resouceImage;
    }
}
