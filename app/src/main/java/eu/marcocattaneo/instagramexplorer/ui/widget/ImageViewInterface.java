package eu.marcocattaneo.instagramexplorer.ui.widget;

import android.support.annotation.DrawableRes;

public interface ImageViewInterface {

    /**
     * Set image resource with media size
     * @param srcMedia
     * @param srcWidth
     * @param srcHeight
     */
    void setSrc(String srcMedia, int srcWidth, int srcHeight);

    void setErrorResouceImage(@DrawableRes int resouceImage);

    void setPlaceholderResouceImage(@DrawableRes int resouceImage);

}
