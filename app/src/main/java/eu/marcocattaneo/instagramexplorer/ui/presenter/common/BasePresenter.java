package eu.marcocattaneo.instagramexplorer.ui.presenter.common;

public interface BasePresenter<V> {

    void onAttach(V view);

    void onDetach();

}
