package eu.marcocattaneo.instagramexplorer.ui.presenter.main;

import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BasePresenter;

public interface MainContract {

    interface View {

        /**
         * Open Login Fragment for Instagram auth
         */
        void openLoginFragment();

        /**
         * Open gallery fragment
         */
        void openGalleryFragment();

    }

    interface MainPresenter extends BasePresenter<View> {

        /**
         * Check current session token
         */
        void checkSession();

    }

}
