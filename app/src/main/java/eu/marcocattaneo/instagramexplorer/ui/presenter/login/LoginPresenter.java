package eu.marcocattaneo.instagramexplorer.ui.presenter.login;

import android.net.Uri;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.di.PerActivity;
import eu.marcocattaneo.instagramexplorer.data.instagramsession.InstagramSession;
import eu.marcocattaneo.instagramexplorer.data.user.User;
import eu.marcocattaneo.instagramexplorer.interactor.login.InstagramLoginCase;
import eu.marcocattaneo.instagramexplorer.interactor.login.ApiProfileCase;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.repository.login.datasource.InstagramSessionRepositoryImpl;
import io.reactivex.observers.DisposableObserver;

@PerActivity
public class LoginPresenter implements LoginContract.LoginPresenter {

    private LoginContract.View mView;
    private InstagramSessionRepository instagramSessionRepository = new InstagramSessionRepositoryImpl();
    private InstagramLoginCase loginCase;

    private ApiProfileCase apiProfileCase;

    @Inject
    public LoginPresenter(InstagramLoginCase instagramLoginCase, ApiProfileCase apiProfileCase) {
        this.loginCase = instagramLoginCase;
        this.apiProfileCase = apiProfileCase;
    }

    @Override
    public void onAttach(LoginContract.View view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        loginCase.dispose();
        mView = null;
    }

    @Override
    public void login() {

        InstagramSession session = instagramSessionRepository.getSetting();
        if (session == null)
            mView.showLoginDialog();
        else
            mView.loginOK(session.getUserName(), session.getProfilePicture());

    }

    /**
     * Require token to instagram
     * @param url
     * @param clientid
     * @param secret
     * @param callback
     */
    @Override
    public void passInstagramCode(String url, String clientid, String secret, String callback) {
        Uri uri = Uri.parse(url);
        String code = uri != null ? uri.getQueryParameter("code") : null;

        if (code != null && !code.isEmpty()) {
            requireToken(code, clientid, secret, callback);
        }
    }

    /**
     * Require Token via http request
     * @param code
     */
    private void requireToken(String code, String clientid, String secret, String callback) {
        InstagramLoginCase.Params params = new InstagramLoginCase.Params(code, clientid, secret, callback);

        loginCase.execute(new LoginDisposable(), params);
    }

    /**
     * Request useProfile
     */
    private void getUserSelf() {
        // Now get userProfile
        apiProfileCase.execute(new UserSelfDisposable(), null);
    }

    /**
     * Disposable for instagram login, Emmit the Token
     */
    private final class LoginDisposable extends DisposableObserver<String> {

        @Override
        public void onNext(String token) {

            // Save session
            InstagramSession session = new InstagramSession();
            session.setToken(token);
            instagramSessionRepository.saveSettings(session);

            getUserSelf();
        }

        @Override
        public void onError(Throwable e) {
            mView.loginKO();
            mView.showSnackbar(e != null ? e.getMessage() : "Login fail");
        }

        @Override
        public void onComplete() {

        }
    }

    /**
     * DIsposable Observer to require use profile
     */
    private final class UserSelfDisposable extends DisposableObserver<User> {

        @Override
        public void onNext(User user) {
            mView.loginOK(user.getFull_name(), user.getProfile_picture());
        }

        @Override
        public void onError(Throwable e) {
            mView.loginKO();
            mView.showSnackbar(e != null ? e.getMessage() : "Login fail");
        }

        @Override
        public void onComplete() {

        }
    }

}
