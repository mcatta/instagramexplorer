package eu.marcocattaneo.instagramexplorer.ui.presenter.gallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.marcocattaneo.instagramexplorer.ExploreApplication;
import eu.marcocattaneo.instagramexplorer.R;
import eu.marcocattaneo.instagramexplorer.data.di.components.ActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.components.DaggerActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.modules.MainModule;
import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.presenter.adapter.ViewPagerGalleryAdapter;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BaseActivity;


public class GalleryActivity extends BaseActivity implements GalleryContract.View {

    public static final String EXTRA_MEDIA_ID = "eu.marcocattaneo.instagramexplorer.media.id";

    @Inject
    public GalleryContract.GalleryPresenter presenter;

    private ViewPagerGalleryAdapter mAdapter;

    @BindView(R.id.viewpager)
    public ViewPager viewPager;

    @BindView(R.id.like_count)
    public TextView likesCountTv;

    @BindView(R.id.msg_count)
    public TextView commentsCountTv;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

        String id = getIntent().getStringExtra(EXTRA_MEDIA_ID);

        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .appComponent( ((ExploreApplication) getApplication()).getAppComponent())
                .mainModule(new MainModule())
                .build();

        activityComponent.inject(this);

        presenter.onAttach(this);

        // Listener viewpager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                presenter.onViewpagerChange(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        presenter.setStartId(id);

        presenter.loadMedias();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    @Override
    public void moveToPosition(int position) {
        viewPager.setCurrentItem(position, false);
    }

    @Override
    public void initAdapter(List<Media> mediaList) {
        mAdapter = new ViewPagerGalleryAdapter(getSupportFragmentManager(), mediaList);
        viewPager.setAdapter(mAdapter);
    }

    @Override
    public void setMediaDetail(int likesCount, int commentCount) {
        likesCountTv.setText(String.valueOf(likesCount));
        commentsCountTv.setText(String.valueOf(commentCount));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
