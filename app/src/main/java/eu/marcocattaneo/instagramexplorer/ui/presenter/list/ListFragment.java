package eu.marcocattaneo.instagramexplorer.ui.presenter.list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import eu.marcocattaneo.instagramexplorer.ExploreApplication;
import eu.marcocattaneo.instagramexplorer.R;
import eu.marcocattaneo.instagramexplorer.data.di.components.ActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.components.DaggerActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.modules.MainModule;
import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.presenter.adapter.ListAdapter;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BaseFragment;
import eu.marcocattaneo.instagramexplorer.ui.presenter.gallery.GalleryActivity;

public class ListFragment extends BaseFragment implements ListContract.View, SwipeRefreshLayout.OnRefreshListener, ListAdapter.OnItemClickListener {

    public static ListFragment newInstance() {

        Bundle args = new Bundle();

        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    public ListContract.ListPresenter presenter;

    private ListAdapter mAdapter;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    @BindView(R.id.gallery_grid)
    public RecyclerView mRecyclerView;

    @BindView(R.id.coordinator)
    public CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.swipe_to_refreh)
    public SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .appComponent( ((ExploreApplication) getActivity().getApplication()).getAppComponent())
                .mainModule(new MainModule())
                .build();

        activityComponent.inject(this);

        presenter.onAttach(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Fetch
        presenter.fetchRemoteMedia(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // refreshlayout
        swipeRefreshLayout.setOnRefreshListener(this);

        initRecyclerView();

        // Load cache
        presenter.loadLocalMedia();
    }

    private void initRecyclerView() {
        // Init grid
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        mRecyclerView.setLayoutManager(staggeredGridLayoutManager);

        // Fix to prevent item moving on scroll
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                ((StaggeredGridLayoutManager)recyclerView.getLayoutManager()).invalidateSpanAssignments();
            }
        });

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.list_padding);
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        // init adapter
        mAdapter = new ListAdapter(new ArrayList<Media>());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    @Override
    public void setData(List<Media> mediaList) {
        mAdapter.swapData(mediaList);
    }

    @Override
    public void startRefresh() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showSnackbar(String msg) {
        Snackbar.make(mCoordinatorLayout, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void onRefresh() {
        presenter.fetchRemoteMedia(true);
    }

    @Override
    public void onClick(View view, Media media) {
        Intent intent = new Intent(getActivity(), GalleryActivity.class);
        intent.putExtra(GalleryActivity.EXTRA_MEDIA_ID, media.getId());
        startActivity(intent);
        getActivity().overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    /**
     * Decorator for recycler view vertical and horizontal spacing
     */
    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space * 2;

            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildLayoutPosition(view) == 0) {
                outRect.top = space * 2;
            } else {
                outRect.top = 0;
            }
        }
    }
}
