package eu.marcocattaneo.instagramexplorer.ui.presenter.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import eu.marcocattaneo.instagramexplorer.ExploreApplication;
import eu.marcocattaneo.instagramexplorer.R;
import eu.marcocattaneo.instagramexplorer.data.di.components.ActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.components.DaggerActivityComponent;
import eu.marcocattaneo.instagramexplorer.data.di.modules.MainModule;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPI;
import eu.marcocattaneo.instagramexplorer.ui.dialog.AuthenticationDialog;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BaseActivity;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BaseFragment;
import eu.marcocattaneo.instagramexplorer.ui.presenter.main.MainContract;
import eu.marcocattaneo.instagramexplorer.ui.widget.CircleTransform;

public class LoginFragment extends BaseFragment implements LoginContract.View {

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private BaseActivity mActivity;

    @BindView(R.id.login_button)
    public Button loginButton;

    @BindView(R.id.coordinator)
    public CoordinatorLayout coordinatorLayout;

    @BindView(R.id.login_name)
    public TextView loginNameTv;

    @BindView(R.id.avatar)
    public ImageView avatarImageView;

    @Inject
    public LoginContract.LoginPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .appComponent( ((ExploreApplication) getActivity().getApplication()).getAppComponent())
                .mainModule(new MainModule())
                .build();

        activityComponent.inject(this);

        mActivity = (BaseActivity) getActivity();

        presenter.onAttach(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    @OnClick(R.id.login_button)
    public void onClickLogin() {

        presenter.login();
    }

    @Override
    public void showSnackbar(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE);
    }

    @Override
    public void showLoginDialog() {
        AuthenticationDialog authenticationDialog = AuthenticationDialog.newInstnace(mActivity,
                InstagramAPI.AUTH_LOGIN + "?client_id=" +  getString(R.string.instagram_clientid) + "&redirect_uri=" + getString(R.string.instagram_callback) + "&response_type=code",
                getString(R.string.instagram_callback));
        authenticationDialog.addOnHttpCallback(new AuthenticationDialog.OnHttpCallback() {
            @Override
            public void onIntercept(WebView webView, String url) {
                loginButton.setEnabled(false);
                presenter.passInstagramCode(url, getString(R.string.instagram_clientid), getString(R.string.instagram_secret), getString(R.string.instagram_callback));
            }
        });
        authenticationDialog.show();
    }

    @Override
    public void loginOK(String loginName, String avatarUrl) {
        loginButton.setVisibility(View.GONE);

        loginNameTv.setText(String.format(getString(R.string.wellcome_text),loginName));

        // Login animation
        Picasso.with(getActivity()).load(avatarUrl)
                .transform(new CircleTransform())
                .into(avatarImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        startAnimation();
                    }

                    @Override
                    public void onError() {

                    }
                });

    }

    @Override
    public void loginKO() {
        loginButton.setEnabled(true);
    }

    /**
     * Animation
     */
    private void startAnimation() {

        final long duration = 4000;

        AlphaAnimation grow = new AlphaAnimation(100, 50);
        grow.setDuration(duration / 2);
        grow.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                /*
                 * On animation end open gallery
                 */
                MainContract.View main = (MainContract.View) getActivity();
                main.openGalleryFragment();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        avatarImageView.setAnimation(grow);
    }
}
