package eu.marcocattaneo.instagramexplorer.ui.presenter.list;

import android.content.Context;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.ui.presenter.common.BasePresenter;

public interface ListContract {

    interface View {

        /**
         * Set media to list
         * @param mediaList
         */
        void setData(List<Media> mediaList);

        /**
         * Show progress
         */
        void startRefresh();

        /**
         * Hide progress
         */
        void stopRefresh();

        /**
         * Show snackbar
         * @param msg
         */
        void showSnackbar(String msg);

        /**
         * Return application context
         * @return
         */
        Context context();

    }

    interface ListPresenter extends BasePresenter<View> {

        /**
         * Load media from local storage
         */
        void loadLocalMedia();

        /**
         * Load media from remote
         * @param force to force refresh
         */
        void fetchRemoteMedia(boolean force);

    }

}
