package eu.marcocattaneo.instagramexplorer.repository.media.datasource;

import java.util.List;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPI;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPIImpl;
import eu.marcocattaneo.instagramexplorer.repository.media.MediaRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.realm.exceptions.RealmError;

public class RemoteDatasource implements MediaRepository {

    private InstagramAPI instagramAPI;

    @Inject
    public RemoteDatasource(InstagramAPI instagramAPI) {
        this.instagramAPI = instagramAPI;
    }

    /**
     * Return media list from web api
     * @return
     */
    @Override
    public Observable<List<Media>> getAll() {
        return Observable.create(new ObservableOnSubscribe<List<Media>>() {

            @Override
            public void subscribe(ObservableEmitter<List<Media>> emitter) throws Exception {

                try {
                    List<Media> medias = instagramAPI.getUserMedia();
                    if (medias != null) {
                        emitter.onNext(medias);
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Exception("No data from api"));
                    }
                } catch (Exception e) {
                    emitter.onError(e);
                }

            }

        });
    }

    @Override
    public void eraseAll() {

    }

    @Override
    public void writeAll(List<Media> mediaList) {

    }
}
