package eu.marcocattaneo.instagramexplorer.repository.media.datasource;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import eu.marcocattaneo.instagramexplorer.repository.media.MediaRepository;
import eu.marcocattaneo.instagramexplorer.utils.JSONMapper;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

public class LocalStorageDatasource implements MediaRepository {

    /**
     * Return observable who query realm db
     * @return Observable
     */
    @Override
    public Observable<List<Media>> getAll() {

        return Observable.create(new ObservableOnSubscribe<List<Media>>() {

            @Override
            public void subscribe(ObservableEmitter<List<Media>> emitter) throws Exception {

                Realm realm = Realm.getDefaultInstance();

                try {
                    List<Media> medias = realm.where(Media.class).findAll();
                    if (medias != null) {
                        emitter.onNext(medias);
                        emitter.onComplete();
                    } else {
                        emitter.onError(new RealmError("No data"));
                    }
                } catch (Exception e) {
                    emitter.onError(new RealmError("Error on query"));
                }

            }

        });
    }

    @Override
    public void eraseAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Media> results = realm.where(Media.class).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    public void writeAll(final List<Media> mediaList) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(mediaList);
            }

        });
    }

}
