package eu.marcocattaneo.instagramexplorer.repository.login;

import eu.marcocattaneo.instagramexplorer.data.user.User;
import io.reactivex.Observable;

public interface LoginRepository {

    /**
     * Return instagram Token
     * @param code
     * @param clientId
     * @param secret
     * @param callback
     * @return
     */
    Observable<String> getInstagramToken(String code, String clientId, String secret, String callback);

    /**
     * Return authenticated user
     * @return
     */
    Observable<User> getUser();

}
