package eu.marcocattaneo.instagramexplorer.repository.login.datasource;

import org.json.JSONObject;

import javax.inject.Inject;

import eu.marcocattaneo.instagramexplorer.data.user.User;
import eu.marcocattaneo.instagramexplorer.http.ApiClientImpl;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPI;
import eu.marcocattaneo.instagramexplorer.http.InstagramAPIImpl;
import eu.marcocattaneo.instagramexplorer.repository.login.LoginRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class InstagramLoginDatasource implements LoginRepository {

    private InstagramAPI instagramAPI;

    @Inject
    public InstagramLoginDatasource(InstagramAPI instagramAPI) {
        this.instagramAPI = instagramAPI;
    }

    @Override
    public Observable<String> getInstagramToken(final String code, final String clientId, final String secret, final String callback) {
        return Observable.create(new ObservableOnSubscribe<String>() {

            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception {



                try {
                    JSONObject jsonObject = instagramAPI.getAuthToken(clientId, secret, callback, code);

                    if (jsonObject != null) {
                        emitter.onNext(jsonObject.getString("access_token"));
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Exception("Object is null"));
                    }
                } catch (Exception e) {
                    emitter.onError(e);
                }

            }

        });
    }

    @Override
    public Observable<User> getUser() {
        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(ObservableEmitter<User> emitter) throws Exception {

                try {

                    User user = instagramAPI.getUserSelf();
                    emitter.onNext(user);
                    emitter.onComplete();

                } catch (Exception e) {
                    emitter.onError(e);
                }

            }
        });
    }

}
