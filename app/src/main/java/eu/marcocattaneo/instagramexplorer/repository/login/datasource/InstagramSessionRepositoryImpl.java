package eu.marcocattaneo.instagramexplorer.repository.login.datasource;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import eu.marcocattaneo.instagramexplorer.data.Constants;
import eu.marcocattaneo.instagramexplorer.data.instagramsession.InstagramSession;
import eu.marcocattaneo.instagramexplorer.repository.login.InstagramSessionRepository;
import eu.marcocattaneo.instagramexplorer.utils.JSONMapper;
import eu.marcocattaneo.instagramexplorer.utils.SharedPrefSingleton;

public class InstagramSessionRepositoryImpl implements InstagramSessionRepository {

    private SharedPreferences sharedPreferences;

    public InstagramSessionRepositoryImpl() {
        sharedPreferences = SharedPrefSingleton.getInstance();
    }

    @Override
    public @Nullable
    InstagramSession getSetting() {

        String json = sharedPreferences.getString(Constants.PREF_INSTAGRAMSESSION, null);

        if (json == null)
            return null;

        return JSONMapper.fromJson(json, InstagramSession.class);
    }

    @Override
    public void saveSettings(@Nullable InstagramSession instagramSession) {
        if (instagramSession != null)
            sharedPreferences.edit().putString(Constants.PREF_INSTAGRAMSESSION, JSONMapper.toJson(instagramSession)).apply();
    }


    @Override
    public void destroy() {
        sharedPreferences.edit().remove(Constants.PREF_INSTAGRAMSESSION).apply();
    }

}
