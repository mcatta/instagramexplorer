package eu.marcocattaneo.instagramexplorer.repository.media;

import java.util.List;

import eu.marcocattaneo.instagramexplorer.data.media.Media;
import io.reactivex.Observable;

public interface MediaRepository {

    /**
     * Return all media
     * @return
     */
    Observable<List<Media>> getAll();

    /**
     * Remove all local data
     */
    void eraseAll();

    /**
     * Write media on db
     * @param mediaList
     */
    void writeAll(List<Media> mediaList);

}
