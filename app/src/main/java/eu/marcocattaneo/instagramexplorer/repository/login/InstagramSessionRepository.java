package eu.marcocattaneo.instagramexplorer.repository.login;

import eu.marcocattaneo.instagramexplorer.data.instagramsession.InstagramSession;

public interface InstagramSessionRepository {

    /**
     * Return current settings
     * @return
     */
    InstagramSession getSetting();

    /**
     * Set new settings
     * @param instagramSession
     */
    void saveSettings(InstagramSession instagramSession);

    /**
     *
     */
    void destroy();

}
